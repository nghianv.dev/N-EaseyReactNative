import React, { Component } from "react";
import {
    View,
    Text, TouchableOpacity
} from "react-native";
import { ButtonBackgroundGreenGradient, ButtonBackgroundBlueGradient } from "./UI";
import {
    BaseReduceCompnentClass,
    BaseReduceCompnentRedux
} from "../BaseReduceCompnent";
import { request, PERMISSIONS, RESULTS } from 'react-native-permissions';

export const Rolling = BaseReduceCompnentRedux(
    (state, stateDefault) => {
        return stateDefault;
    },
    (dispatch, dispatchDefault) => {
        return dispatchDefault;
    },

    class extends BaseReduceCompnentClass {
        constructor(props) {
            super(props);
            this.state = {
                showThemModal: false
            }
        }

        componentDidMount() {

        }

        async checkingCamera() {
            let result = await request(PERMISSIONS[Platform.OS.toUpperCase()].CAMERA);
            switch (result) {
                case RESULTS.UNAVAILABLE:
                    displayError('This feature is not available (on this device / in this context)', () => {
                        
                    });
                    break;
                case RESULTS.DENIED:
                    displayError('The permission has not been requested / is denied but requestable', () => {
                        
                    });
                    break;
                case RESULTS.GRANTED:
                    setTimeout(() => {
                        this.props.navigation.navigate('ScanQrCode');
                    }, 500);
                    break;
                case RESULTS.BLOCKED:
                    displayError('The permission is denied and not requestable anymore', () => {
                        
                    });
                    break;
            }
        }

        render() {
            return (
                <View style={{ flex: 1, backgroundColor: '#fff', padding: 30, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#41ac96', fontSize: 40, fontWeight: 'bold' }}>I-Tracking</Text>
                    <Text style={{ marginTop: 30, fontSize: 13, color: '#1e1e1e' }}>Tạo và tra cứu thông tin mã QR CODE</Text>

                    <ButtonBackgroundGreenGradient style={{ width: '100%', padding: 10, marginTop: 100, marginBottom: 20 }} text="Tôi là quản lý"
                        onPress={() => {
                            this.props.navigation.navigate('AdminManager');
                        }} />
                    <ButtonBackgroundBlueGradient onPress={() => {
                        this.checkingCamera();
                    }} style={{ width: '100%', padding: 10 }} text="Tôi là khách hàng" />
                </View>
            );
        }
    }
);

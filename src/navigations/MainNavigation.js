import { StackNavigator, StackViewTransitionConfigs } from "react-navigation";

import { Rolling } from "../components/Rolling";

const MainNavigation = StackNavigator(
  {
    Rolling: { screen: Rolling },
  
  },
  {
    headerMode: "none",
    transitionConfig: () =>
      StackViewTransitionConfigs.SlideFromRightIOS
  }
);

export default MainNavigation;
